
// this directive is app entry point
  
var template = require('raw!templates/app-a.html');

module.exports = function(app) {
  'use strict';
  
  app.directive('fedAppA', ['$fedAppCommon', function($fedAppCommon) {
    return {
      scope    : true,
      template : template,
      link     : function (scope, el, attr) {
        scope.users = $fedAppCommon.appData.users;
      }
    };
  }]);
}