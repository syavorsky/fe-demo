
// assume global `angular` exists

require('style!styles/app-a.scss');

var App = angular.module('fedAppA', ['fedCommon', 'ngSanitize']);

[
  require('./directives/fed-app-a')
].forEach(function(define) {
  define(App);
})

module.exports = App;
