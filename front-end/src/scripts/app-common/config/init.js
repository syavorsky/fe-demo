
module.exports = function(app) {
  'use strict';

  app.config(['$fedAppCommonProvider', function($fedAppCommonProvider) {
    var initEl = document.querySelector('script[type="text/fed-app-common-config"]');
    if (initEl && initEl.innerHTML) {
      try {
        var data = JSON.parse(initEl.innerHTML);
        $fedAppCommonProvider.init(data);
        (console && console.log('Started with:', data));
      } catch (err) {
        // $log is not available in providers
        console.log(err);
        (console && console.warn('Failed to parse initial data:', initEl.innerHTML));
      }
    } else {
      (console && console.log('No initial data'));
    }

  }]);
};