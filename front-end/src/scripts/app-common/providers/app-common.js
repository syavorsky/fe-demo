
module.exports = function(app) {
  'use strict';

  app.provider('$fedAppCommon', function() {

    var data = null;

    return {

      init: function init(d) {
        data = Object.freeze ? Object.freeze(d) : d;
      },

      $get: function() {
        return data;
      }
    };
  });
};