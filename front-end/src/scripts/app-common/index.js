
require('jquery');
require('angular');
require('angular-sanitize');
require('ui-toolkit');

var App = angular.module('fedCommon', ['ngSanitize']);

require('style!styles/app-common.scss');

[
  require('./config/init'),
  require('./providers/app-common'),
  require('./directives/link')
].forEach(function(define) {
  define(App);
});

module.exports = App;