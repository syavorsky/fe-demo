
module.exports = function(app) {
  'use strict';

  app.directive('fedLink', ['$rootScope', '$fedAppCommon', function($rootScope, $fedAppCommon) {
    return {
      link: function(scope, el, attrs) {
        var site = $fedAppCommon.CONFIG && $fedAppCommon.CONFIG.baseUrl || '';
        el.attr('href', site + attrs.fedLink);
      }
    }
  }]);
}