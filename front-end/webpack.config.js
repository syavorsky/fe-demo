
var webpack  = require('webpack');
var optimize = !!process.env.FED_OPTIMZE;

module.exports = {
  devtool : 'source-map',
  entry : {
    'app-common' : ['app-common'],
    'app-a'      : ['app-a'],
  },
  output    : {
    path     : 'front-end/dist/',
    filename : '[name].js'
  },
  resolve   : {
    modulesDirectories : ['front-end/src', 'front-end/src/scripts', 'node_modules']
  },
  module    : {
    loaders : [{
      test    : /\.js$/, 
      loaders : [],
      exclude : /node_modules/
    }, {
      test    : /\.scss$/,
      loaders : ['css', 'autoprefixer', 'sass']
    }],
    noParse : [
      './node_modules/'
    ]
  },
  plugins: [
    optimize ? (new webpack.optimize.UglifyJsPlugin({minimize: true})) : null
  ].filter(function(p) { return !!p; })
};