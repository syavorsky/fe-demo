# Front-End Demo setup

## Setup

Install dev and app dependencies . All dependencies are declared in `devDependencies` since they are needed only ar bundling time.


```
> npm install
```

## Run

Start "back-end", any way serving static files works, then go to `http://127.0.0.1:8080/back-end/app-a/`. For example you may run

```
python -m SimpleHTTPServer
```

Start build watcher as `npm start` or run build process once with `npm run bundle`.